package rajiv.com.placeproject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;

import java.util.List;

import rajiv.com.placeproject.Permission.CheckPermissionUtil;
import rajiv.com.placeproject.Permission.PermissionUtil;


public class GetCurrentPlaceActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private final String TAG = "GetCurrentPlaces";

    private final int RESOLVE_CONNECTION_REQUEST_CODE = 1000;

    private GoogleApiClient mGoogleApiClient;

    private PlaceLikelihood mostLikelyPlace = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_current_place);

        findViewById(R.id.btnGetCurPlace).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getCurrentPlaceData();
                    }
                });

        // Build the GoogleApiClient and connect to the Places API
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        PermissionUtil.onRequestPermissionResult(this, requestCode, permissions, grantResults);

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            // If there was a problem connecting to the Play Services library and
            // we were able to resolve it, this code will passed in and
            // we can then connect to the library
            case RESOLVE_CONNECTION_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    mGoogleApiClient.connect();
                }
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "Connection was suspended for some reason: " + cause);
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.d(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());

        // Check to see if there's a way to resolve this problem. Usually the user can just
        // update Play Services and be on their way.
        if (result.hasResolution()) {
            try {
                result.startResolutionForResult(this, RESOLVE_CONNECTION_REQUEST_CODE);
            } catch (IntentSender.SendIntentException e) {
                // Unable to resolve - use this opportunity to message user appropriately
                Log.e(TAG, "Could not connect to Play Services");
            }
        } else {
            // If there's no resolution available to the problem, give the user an error dialog
            // (which will typically tell them how to fix what's wrong)
            GoogleApiAvailability gAPI = GoogleApiAvailability.getInstance();
            gAPI.getErrorDialog(this, result.getErrorCode(), 0).show();
        }
    }

    //TODO Important method
    private void getCurrentPlaceData() {

        CheckPermissionUtil.checkLocation(this, new PermissionUtil.ReqPermissionCallback() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onResult(boolean success) {

                        if (success) {

                            PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi.getCurrentPlace(mGoogleApiClient, null);
                            result = Places.PlaceDetectionApi.getCurrentPlace(mGoogleApiClient, null);
                            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                                @Override
                                public void onResult(@NonNull PlaceLikelihoodBuffer placeLikelihoods) {

                                    for (PlaceLikelihood placeLikelihood : placeLikelihoods) {
                                        Log.d(TAG, String.format("Place %s has likelihood : %g", placeLikelihood.getPlace().getName(), placeLikelihood.getLikelihood()));

                                        if (mostLikelyPlace == null) {
                                            mostLikelyPlace = placeLikelihood;
                                        } else {
                                            if (placeLikelihood.getLikelihood() > mostLikelyPlace.getLikelihood()) {
                                                mostLikelyPlace = placeLikelihood;
                                            }
                                        }
                                    }

                                    if (mostLikelyPlace != null) {
                                        Place tempPlace = mostLikelyPlace.getPlace();
                                        String str = tempPlace.getName() + "\n"
                                                + tempPlace.getAddress() + "\n"
                                                + tempPlace.getPhoneNumber() + "\n";

                                        if (tempPlace.getWebsiteUri() != null)
                                            str += tempPlace.getWebsiteUri() + "\n";

                                        List<Integer> typeList = tempPlace.getPlaceTypes();
                                        str += "Types: " + typeList.toString();

                                        TextView tvPlace = (TextView) findViewById(R.id.tvPlaceInfo);
                                        tvPlace.setText(str);
                                    }


                                    final CharSequence attribs = placeLikelihoods.getAttributions();
                                    Log.d(TAG, "TEST  " + attribs);
                                    if (attribs != null && attribs.length() > 0) {
                                        String attrStr = attribs.toString();
                                        TextView tvAttribs = (TextView) findViewById(R.id.tvAttributions);
                                        tvAttribs.setText(Html.fromHtml(attrStr));
                                        // make the link clickable
                                        tvAttribs.setMovementMethod(LinkMovementMethod.getInstance());
                                    }

                                    placeLikelihoods.release();

                                }
                            });

                        } else {
                            Toast.makeText(GetCurrentPlaceActivity.this, "Permission has  been declined", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
        );

    }
}
