package rajiv.com.placeproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class PlaceAutocompleteActivity extends AppCompatActivity {

    private final String TAG = "PLACECOMPLETE_EXERCISE";
    private final int PLACE_AUTOCOMPLETE_REQUEST = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_autocomplete);

        findViewById(R.id.btnTriggerAutoComp).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectAPlace();
                    }
                }
        );

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place Selected: " + place.getName());

                updateUI(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e(TAG, "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
                updateUI(null);
            }
        }
    }


    //TODO Important method
    protected void selectAPlace() {
        try {
            Intent intent;

            // A filter can be used to restrict the kinds of predictions that
            // the autocomplete widget provides. In this case, the code
            // only displays places that are business establishments
            // TODO: Create autocomplete filter
            AutocompleteFilter filter = new AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                    .build();

            // Mode can either be MODE_FULLSCREEN or MODE_OVERLAY
            // TODO: Create the Autocomplete IntentBuilder
            intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setBoundsBias(new LatLngBounds.Builder() //Todo used bound for area restricted search
                            .include(new LatLng(47.608670, -122.340033))
                            .include(new LatLng(47.610005, -122.342865)).build())
                    .setFilter(filter) //Todo used filter
                    .build(this);

            // Start the Autocomplete Activity. the result will be returned
            // in the onActivityResult function
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    //Todo set result to UI
    private void updateUI(Place chosenPlace) {
        TextView placeData = (TextView) findViewById(R.id.tvPlaceData);

        if (chosenPlace == null) {
            placeData.setText("No place selected");
        } else {
            String str = chosenPlace.getName() + "\n"
                    + chosenPlace.getAddress() + "\n"
                    + chosenPlace.getPhoneNumber() + "\n"
                    + chosenPlace.getWebsiteUri();

            placeData.setText(str);
        }
    }

}
