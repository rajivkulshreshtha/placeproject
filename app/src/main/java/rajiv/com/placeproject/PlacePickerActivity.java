package rajiv.com.placeproject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import rajiv.com.placeproject.Permission.CheckPermissionUtil;
import rajiv.com.placeproject.Permission.PermissionUtil;

public class PlacePickerActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private final String TAG = "PLACEPICKER";

    private final int RESOLVE_CONNECTION_REQUEST_CODE = 1000;
    private final int PLACE_PICKER_REQUEST = 1001;

    protected GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_picker);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        findViewById(R.id.btnTriggerPlacePick).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pickAPlace();
                    }
                }
        );

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            // This code is passed when the user has resolved whatever connection
            // problem there was with the Google Play Services library
            case RESOLVE_CONNECTION_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    mGoogleApiClient.connect();
                }
                break;

            case PLACE_PICKER_REQUEST:
                if (resultCode == RESULT_OK) {
                    Place chosenPlace = PlacePicker.getPlace(this, data);
                    updateUI(chosenPlace);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        PermissionUtil.onRequestPermissionResult(this, requestCode, permissions, grantResults);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.d(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
        if (result.hasResolution()) {
            try {
                result.startResolutionForResult(this, RESOLVE_CONNECTION_REQUEST_CODE);
            } catch (IntentSender.SendIntentException e) {
                // Unable to resolve, message user appropriately
            }
        } else {
            GoogleApiAvailability gAPI = GoogleApiAvailability.getInstance();
            gAPI.getErrorDialog(this, result.getErrorCode(), 0).show();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "Connection was suspended for some reason");
        mGoogleApiClient.connect();
    }


    //TODO Important method
    private void pickAPlace() {

        CheckPermissionUtil.checkLocation(this, new PermissionUtil.ReqPermissionCallback() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onResult(boolean success) {

                        if (success) {

                            //Todo Use this if you want to add specific location, Else comment this
                            LatLngBounds bounds = new LatLngBounds.Builder()
                                    .include(new LatLng(47.608670, -122.34003))
                                    .include(new LatLng(47.61005, -122.342865))
                                    .build();


                            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                            Intent intent = null;
                            try {

                                //Todo Use this if you want to use users current location, Else comment this
                                //intent = builder.build(PlacePickerActivity.this);

                                //Todo Use this if you want to add specific location, Else comment this
                                intent = builder.setLatLngBounds(bounds).build(PlacePickerActivity.this);

                            } catch (GooglePlayServicesRepairableException e) {
                                e.printStackTrace();
                            } catch (GooglePlayServicesNotAvailableException e) {
                                e.printStackTrace();
                            }
                            startActivityForResult(intent, PLACE_PICKER_REQUEST);

                        } else {
                            Toast.makeText(PlacePickerActivity.this, "Permission has  been declined", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
        );


    }

    //TODO This method set the results to UI
    private void updateUI(Place chosenPlace) {
        TextView placeData = (TextView) findViewById(R.id.tvPlaceData);

        String str = chosenPlace.getName() + "\n"
                + chosenPlace.getAddress() + "\n"
                + chosenPlace.getPhoneNumber() + "\n"
                + chosenPlace.getWebsiteUri();

        placeData.setText(str);
    }

}
